'use script';
//assignment 7.0 - write a program to print permutations of an (input) string
//jeg har ikke kunne få input til at fungere... Så det bliver uden. 

const perms = (ab) => {
   if (ab.length ===1) return ab;

   const results=[]

   for(let i=0; i<ab.length; i++) {
      const currentChar = ab[i]
      const remainingsChar=ab.slice(0,i) + ab.slice(i+1)

      for (let permutation of perms(remainingsChar)) {
         results.push(currentChar + permutation)
      }
   }
   console.log(results)
   return results
}
let ab= 'anna'
perms(ab)

